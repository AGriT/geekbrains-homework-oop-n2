#include "CPerson.h"

CPerson::CPerson(string strName, int nAge, bool blSex, int nWeight)
				:	m_strName(strName),
					m_nAge(nAge),
					m_blSex(blSex),
					m_nWeight(nWeight)
{
}

CPerson::CPerson()
{
	m_strName = "John";
	m_nAge = 21;
	m_blSex = true;
	m_nWeight = 65;
}


CPerson::~CPerson()
{
}
