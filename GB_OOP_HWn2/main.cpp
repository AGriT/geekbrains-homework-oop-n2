#include <iostream>
#include "CStudent.h"
#include "CGrannySmith.h"
#include "CBanana.h"

static const int sc_nAmmount = 5;

int CStudent::m_nCount = 0;

int main()
{
	CStudent* ptrclClass[sc_nAmmount];

	string strSearchname = "Emy";
	
	ptrclClass[0] = new CStudent("Alex", 21, true, 65, 3);
	ptrclClass[1] = new CStudent("Emy", 18, false, 58, 1);
	ptrclClass[2] = new CStudent("Ivan", 25, true, 90, 5);
	ptrclClass[3] = new CStudent("Victor", 19, true, 74, 2);
	ptrclClass[4] = new CStudent();

	for (int i = 0; i < sc_nAmmount; i++)
	{
		if (ptrclClass[i]->nCompare(strSearchname) == 0)
			ptrclClass[i]->vPrintInfo();
	}

// ------------------------------Second Task--------------------------------
	CApple a("red");
	CBanana b;
	CGrannySmith c;

	std::cout << "My " << a.strGetName() << " is " << a.strGetColor() << ".\n";
	std::cout << "My " << b.strGetName() << " is " << b.strGetColor() << ".\n";
	std::cout << "My " << c.strGetName() << " is " << c.strGetColor() << ".\n";

	return 0;
}