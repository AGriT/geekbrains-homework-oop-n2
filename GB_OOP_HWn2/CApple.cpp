#include "CApple.h"

CApple::CApple(string strName, string strColor)
				:	CFruit(strName, strColor)
{
}

CApple::CApple(string strColor)
				:	CFruit("Apple", strColor)
{
}


CApple::CApple()
				:	CFruit("Apple", "Green")
{
}


CApple::~CApple()
{
}
