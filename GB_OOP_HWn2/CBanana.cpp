#include "CBanana.h"

CBanana::CBanana(string strName, string strColor)
				:	CFruit(strName, strColor)
{
}

CBanana::CBanana(string strColor)
				:	CFruit("Banana", strColor)
{
}

CBanana::CBanana()
				:	 CFruit("Banana", "Yellow")
{
}


CBanana::~CBanana()
{
}
