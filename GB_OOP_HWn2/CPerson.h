#pragma once
#include <string>

using namespace std;

class CPerson
{
public:
	CPerson(string, int, bool, int);
	CPerson();
	~CPerson();

	inline void vRename(string strName) { m_strName = strName; };
	inline void vSetAge(int nAge) { m_nAge = nAge; };
	inline void vSetWeight(int nWeight) { m_nWeight = nWeight; };

	inline int nCompare(string strName) { return strcmp(m_strName.c_str(), strName.c_str()); };

protected:
	string m_strName;
	int m_nAge;
	bool m_blSex;
	int m_nWeight;
};

