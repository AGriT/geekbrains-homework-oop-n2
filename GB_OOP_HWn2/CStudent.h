#pragma once
#include <iostream>
#include "CPerson.h"

class CStudent :
	public CPerson
{
public:
	CStudent(string, int, bool, int, int);
	CStudent();
	~CStudent();

	inline int nGetYear() { return m_nYear; };
	inline void vSetYear(int nYear) { m_nYear = nYear; };
	inline void vIncreaseYear() { m_nYear++; };
	void vPrintInfo();

	static int nGetCounter() { return CStudent::m_nCount;};
	static void nChangeCounter(int nCount) { m_nCount = nCount; };

private:
	int m_nYear;
	static int m_nCount;
};

