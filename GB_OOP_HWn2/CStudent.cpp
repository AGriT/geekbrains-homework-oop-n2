#include "CStudent.h"

CStudent::CStudent(string strName, int nAge, bool blSex, int nWeight, int nYear)
					:	CPerson(strName, nAge, blSex, nWeight),
						m_nYear(nYear)
{
	m_nCount++;
}

CStudent::CStudent()
					:	CPerson(),
						m_nYear(1)
{
	m_nCount++;
}


CStudent::~CStudent()
{
}

void CStudent::vPrintInfo()
{
	cout << "Name: " << m_strName << endl;
	cout << "Age: "<< m_nAge << endl;
	cout << "Sex: " << ((m_blSex) ? "Male\n" : "Female\n"); 
	cout << "Weight: "<< m_nWeight << endl;
	cout << "Year: " << m_nYear << endl;
}