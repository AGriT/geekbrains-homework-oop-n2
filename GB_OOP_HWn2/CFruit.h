#pragma once
#include <string>
using namespace std;

class CFruit
{
public:
	CFruit(string, string);
	CFruit();
	virtual ~CFruit();

	string strGetName() { return m_strName; };
	string strGetColor() { return m_strColor; };

protected:
	string m_strName;
	string m_strColor;
};

